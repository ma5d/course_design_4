from varible import cur, list_y, matplot_chinese
import matplotlib.pyplot as plt

matplot_chinese()
list_x = ["中 专", "中 技", "初 中 及 以 下", "高 中", "大 专", "在 校 生 / 应 届 生", "本 科", "硕 士", "博 士"]
for i in list_x:
    cur.execute("""
        select count(*) from 51job_table where attribute_text like "%{}%" limit 10;
        """.format(i))
    list_y.append(cur.fetchall()[0][0])
cur.execute("""select count(*) from `51job_table`""")
list_x.insert(0, "无要求")
list_y.insert(0, cur.fetchall()[0][0])
plt.figure(figsize=(5, 5))
plt.pie(x=list_y, labels=list_x, radius=1)
plt.show()