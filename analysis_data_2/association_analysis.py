from varible import cur
import pandas as pd
from mlxtend.preprocessing import TransactionEncoder
from mlxtend.frequent_patterns import apriori
# 对年薪超过30万的岗位进行关联挖掘，分析这些高薪岗位关联的工作地点和需求专业信息；
# 设置数据集

cur.execute("""
    select providesalary_text, substring_index(workarea_text, "-", 1), 
    substring_index(substring_index(attribute_text, "   ", 3), "   ", -1)
    from 51job_table
    where convert(providesalary_text, signed)*12>300 
    limit 10;
    """)
dataset = cur.fetchall()
te = TransactionEncoder()
# 进行 one-hot 编码
te_ary = te.fit(dataset).transform(dataset)
df = pd.DataFrame(te_ary, columns=te.columns_)
# 利用 Apriori 找出频繁项集
freq = apriori(df, min_support=0.3, use_colnames=True, max_len=3)
print(freq)
