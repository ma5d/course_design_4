from varible import cur, SALARY_WORKTIME, list_x, list_y
import matplotlib.pyplot as plt

cur.execute("""
select count(*), convert(providesalary_text, signed) as data
from `51job_table` 
group by data
order by data""")

for i in cur.fetchall():
    list_y.append(i[0])
    list_x.append(i[1])

print(list_x, "\n", list_y)
plt.figure(figsize=(16, 8))
plt.xlim(0, 80)
plt.ylim(0, 10000)
plt.bar(list_x, list_y, color="orange")
plt.show()

