from varible import cur, plt, matplot_chinese
import numpy as np
import seaborn as sns
matplot_chinese()

cur.execute("""
    select job_name
    from `51job_table`
    group by job_name
    order by count(*) desc
    limit 10;
    """)
list_x = ["中 专", "中 技", "初 中 及 以 下", "高 中", "大 专", "在 校 生 / 应 届 生", "本 科", "硕 士", "博 士"]
list_y = list(i[0] for i in cur.fetchall())
heapdata = np.zeros(shape=(len(list_y), len(list_x)))
for i in range(len(list_x)):
    for j in range(len(list_y)):
        cur.execute("""
            select count(*)
            from `51job_table`
            where job_name='{}' and attribute_text like '%{}%';
            """.format(list_y[j], list_x[i]))
        heapdata[j][i] = int(cur.fetchall()[0][0])

list_x = list(i.replace(" ", "") for i in list_x)
list_y = list(i.split("/")[0] for i in list_y)
heatmap = sns.heatmap(heapdata, cmap="hot_r", vmin=-100, vmax=500, annot=True, xticklabels=list_x, yticklabels=list_y)
plt.show()
