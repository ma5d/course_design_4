from varible import cur, matplot_chinese, plt, list_x, list_y
from sklearn.cluster import KMeans
matplot_chinese()
import numpy as np
import pandas as pd

if __name__=='__main__':
    cur.execute("""select workarea_text, providesalary_text from `51job_table`;""")
    for i in cur.fetchall():
        list_y.append(i[0])
        list_x.append(float(i[1]))
    array_x = np.array(list_x).reshape(-1, 1)
    kmodel = KMeans(n_clusters=6, n_jobs=4)
    kmodel.fit(X=array_x, y=list_y)
    num = pd.Series(kmodel.labels_).value_counts()
    print(list(kmodel.labels_), "\n", list_y)
    print(num)